# Artsdata written in React

## Setup
You need to have MongoDB running.

* ```npm install```
* ```npm start```

if this is the first time you run the server, you need to populate the database: ```npm run serverinit```

After the first time you can run the server with ```npm run server```
