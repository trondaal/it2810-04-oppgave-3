import React from 'react'
import ReactDOM from 'react-dom'

import { Router, Route, browserHistory, IndexRoute } from 'react-router'

import App from './App'
import styles from './index.css'
import LoginContainer from './components/login/LoginContainer'
import RegistrerContainer from './components/login/RegistrerContainer'
import ContentContainer from './components/content/ContentContainer'

ReactDOM.render (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={ContentContainer} />
      <Route path="/login" component={LoginContainer} />
      <Route path="/registrer" component={RegistrerContainer} />
    </Route>
  </Router>,
  document.querySelector('#root')
)
