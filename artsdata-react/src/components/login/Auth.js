import React, { Component } from 'react'
/**
* The Auth class checks if the user is logged in and shows the user his/her Observations in green highlight
*
* @class Auth
* @extends {Component}
*
*/
class Auth extends Component {
  /**
  * Creates an instance of Auth
  *
  * @param: {any} props
  */
  constructor (props) {
    super(props)
    this.state = {
      loggedin: !!(localStorage['user'] && localStorage['user'].length)
    }
  }

  render () {
    let content = null
    if (!!(localStorage['user'] && localStorage['user'].length))
      content = this.props.children

    return (
      <div className="auth-container auth">
        {content}
      </div>
    )
  }
}

export default Auth
