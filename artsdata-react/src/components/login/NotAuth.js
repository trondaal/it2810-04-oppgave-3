import React, { Component } from 'react'
/**
* Checks if the user is logged in and hides any option for adding observations if the user is logged out
*
* @class NotAuth
* @extends {Component}
*/
class NotAuth extends Component {
  /**
  * Creates an instance the Notauth
  *
  * @param: {any} props
  *
  */
  constructor (props) {
    super(props)
    this.state = {
      loggedin: !!(localStorage['user'] && localStorage['user'].length)

    }
  }

  render () {
    let content = null
    if (!(localStorage['user'] && localStorage['user'].length))
      content = this.props.children

    return (
      <div className="not-auth-container not-auth">
        {content}
      </div>
    )
  }
}

export default NotAuth
