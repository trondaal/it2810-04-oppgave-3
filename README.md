# Sjeldne arter #
version 1.0

### Summary ###

The "Sjeldne arter" project is used to register and view observations of rare species in Norway. It is based on data from [Artsdatabanken](http://www.artsdatabanken.no/) and their [web API](http://artskart2.artsdatabanken.no/help)

### Requirements ###

The application consists of a NodeJS server powered with Express, and a backend document database powered by MongoDB.

* [NodeJS](https://nodejs.org/)
* [MongoDB](https://docs.mongodb.com/)
* A browser for testing. Chrome is recommended.

### Instructions ###

* To get started you need to install MongoDB on your mahcine. We have used this  [tutorial](https://docs.mongodb.com/manual/tutorial/) for installation and setup on Windows and OS X.

* After the database service is installed you need to start it (see the same tutorial as installation).

* Then you must install NodeJS and clone this repository to your local machine.

* Navigate to the "artsdata-react" folder in a command line tool and run the command "npm install" to install all the necessary depencencies for the project.

* Now you can start the project by runnning "npm start" in the command line and browsing to [http://localhost:3000](http://localhost:3000). If by any chance the project hasn't started you should try the command "node server.js" after "npm start". Or contact one of us if that still doesn't work!

### Contact ###

* Andreas Aursand
* Helene Engeness M�rk
* H�vard Tollefsen
* Niklas Molnes Hole
* Thomas Wold
* Torjus Sandviken